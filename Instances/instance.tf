provider "aws" {
  region = "ap-south-1" # Change to your desired region
  profile = "default"
  shared_config_files      = ["/home/zubair/Documents/Terraform/Credentials/config"]
  shared_credentials_files = ["/home/zubair/Documents/Terraform/Credentials/credentials"]
}
#################### # Fetch the details using the data source ###################
# Fetch the VPC  details using the data source
data "aws_vpc" "two-tier-vpc" {
  filter {
    name   = "tag:Name"
    values = ["two-tier-vpc"]
  }
}
data "aws_subnet" "public-subnet-1" {
  filter {
    name   = "tag:Name"
    values = ["two-tier-pub-sub-1"]
  }
}
data "aws_subnet" "public-subnet-2" {
  filter {
    name   = "tag:Name"
    values = ["two-tier-pub-sub-2"]
  }
}
data "aws_subnet" "private-subnet" {
  filter {
    name   = "tag:Name"
    values = ["two-tier-priv-sub-8"]
  }
}
# Fetch the security group details using the data source
data "aws_security_group" "public_sg" {
  filter {
    name   = "tag:Name"
    values = ["two-tier-public-sg"]
  } 
}
data "aws_security_group" "Private_sg" {
  filter {
    name   = "tag:Name"
    values = ["two-tier-private-sg"]
  } 
}

################### create Key pair ###################
# RSA key of size 4096 bits
resource "tls_private_key" "rsa-4096" {
  algorithm = "RSA"
  rsa_bits  = 4096
}
resource "aws_key_pair" "key_pair" {
  key_name   = "key_pair"
  public_key = tls_private_key.rsa-4096.public_key_openssh
}

resource "local_file" "private_key" {
  content = tls_private_key.rsa-4096.private_key_pem
  filename = "key_pair.pem"
}
################### Create resources in public subnet ###################
resource "aws_instance" "public_instance" {
  ami           = "ami-0a7cf821b91bcccbc"  # Replace with your desired AMI ID
  instance_type = "t2.micro"
  subnet_id     = data.aws_subnet.public-subnet-1.id
  vpc_security_group_ids = [data.aws_security_group.public_sg.id]
  key_name      = "key_pair"
  depends_on = [aws_key_pair.key_pair]
  source_dest_check = false
  tags = {
    Name = "two-tier-public-instance"
    backup = "Yes"
    group = "web"
  }

  user_data = <<-EOF
              #!/bin/bash
              sudo apt-get update
              sudo apt-get install -y apache2
              echo "Hello, World! public server " > /var/www/html/index.html
              EOF
  # Other instance configuration...
}

################### Create resources in private subnet ###################
resource "aws_instance" "private_instance" {
  ami           = "ami-0a7cf821b91bcccbc"  # Replace with your desired AMI ID
  instance_type = "t2.micro"
  subnet_id     = data.aws_subnet.public-subnet-2.id
  vpc_security_group_ids = [data.aws_security_group.Private_sg.id]
  key_name      = "key_pair"
  depends_on = [aws_key_pair.key_pair]
  tags = {
    Name = "two-tier-private-instnace"
    backup = "Yes"
    group = "web"
  }

  user_data = <<-EOF
              #!/bin/bash
              sudo apt-get update
              sudo apt-get install -y apache2
              echo "Hello, World! private server" > /var/www/html/index.html
              EOF
  # Other instance configuration...
}

resource "aws_instance" "private_instance_02" {
  ami           = "ami-0a7cf821b91bcccbc"  # Replace with your desired AMI ID
  instance_type = "t2.micro"
  subnet_id     = data.aws_subnet.public-subnet-1.id
  vpc_security_group_ids = [data.aws_security_group.Private_sg.id]
  key_name      = "key_pair"
  depends_on = [aws_key_pair.key_pair]
  tags = {
    Name = "two-tier-private-instnace_02"
    backup = "Yes"
    group = "web"
  }

  user_data = <<-EOF
              #!/bin/bash
              sudo apt-get update
              sudo apt-get install -y apache2
              echo "Hello, World! private server No 2" > /var/www/html/index.html
              EOF
  # Other instance configuration...
}

