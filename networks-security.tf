################### Public Security Group ###################
resource "aws_security_group" "two-tier-ec2-sg" {
  name        = "two-tier-public-sg"
  description = "Allow all traffic"
  vpc_id      = aws_vpc.three_tier_VPC.id
  depends_on = [
    aws_vpc.three_tier_VPC
  ]

  ingress {
    from_port = "80"
    to_port   = "80"
    protocol  = "tcp"
#    cidr_blocks = ["0.0.0.0/0"]
    security_groups  = [aws_security_group.two-tier-alb-sg.id]
  }
  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
    ingress {
    from_port   = "44"
    to_port     = "443"
    protocol    = "tcp"
#    cidr_blocks = ["0.0.0.0/0"]
    security_groups  = [aws_security_group.two-tier-alb-sg.id]
  }
  ingress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "icmp"
    security_groups  = [aws_security_group.two-tier-alb-sg.id]
  }

  egress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "tcp"
    cidr_blocks = ["122.171.86.78/32"]
  }
     egress {
    from_port = "80"
    to_port   = "80"
    protocol  = "tcp"
#    cidr_blocks = ["0.0.0.0/0"]
    security_groups  = [aws_security_group.two-tier-alb-sg.id]
  }
    egress {
    from_port   = "44"
    to_port     = "443"
    protocol    = "tcp"
#    cidr_blocks = ["0.0.0.0/0"]
    security_groups  = [aws_security_group.two-tier-alb-sg.id]
  }
  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "icmp"
    security_groups  = [aws_security_group.two-tier-alb-sg.id]
  }
    egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["10.0.8.0/24"]
  }

  tags = {
    Name = "two-tier-public-sg"
  }
}

################### Private Security Group ###################
resource "aws_security_group" "two-tier-NAT-sg" {
  name        = "two-tier-Private-sg"
  description = "Allow traffic from public subnet and only allowed resources"
  vpc_id      = aws_vpc.three_tier_VPC.id
  depends_on = [
    aws_vpc.three_tier_VPC
  ]

  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "tcp"
    cidr_blocks = ["10.0.1.0/24"]
  }
    ingress {
    from_port = "80"
    to_port   = "80"
    protocol  = "tcp"
#    cidr_blocks = ["0.0.0.0/0"]
    security_groups  = [aws_security_group.two-tier-alb-sg.id]
  }
    ingress {
    from_port   = "443"
    to_port     = "443"
    protocol    = "tcp"
#    cidr_blocks = ["0.0.0.0/0"]
    security_groups  = [aws_security_group.two-tier-alb-sg.id]
  }
  ingress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "icmp"
    security_groups  = [aws_security_group.two-tier-alb-sg.id]
  }
   egress {
    from_port = "80"
    to_port   = "80"
    protocol  = "tcp"
#    cidr_blocks = ["0.0.0.0/0"]
    security_groups  = [aws_security_group.two-tier-alb-sg.id]
  }
    egress {
    from_port   = "44"
    to_port     = "443"
    protocol    = "tcp"
#    cidr_blocks = ["0.0.0.0/0"]
    security_groups  = [aws_security_group.two-tier-alb-sg.id]
  }
  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "icmp"
    security_groups  = [aws_security_group.two-tier-alb-sg.id]
  }
    egress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "tcp"
    cidr_blocks = ["10.0.1.0/24"]
  }
   egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "two-tier-private-sg"
  }
}

################### Load balancer security group ###################
resource "aws_security_group" "two-tier-alb-sg" {
  name        = "two-tier-alb-sg"
  description = "load balancer security group"
  vpc_id      = aws_vpc.three_tier_VPC.id
  depends_on = [
    aws_vpc.three_tier_VPC
  ]


  ingress {
    from_port   = "80"
    to_port     = "80"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
    ingress {
    from_port   = "443"
    to_port     = "443"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
    ingress {
    from_port   = "80"
    to_port     = "80"
    protocol    = "tcp"
    cidr_blocks = ["10.0.1.0/24"]
  }
    ingress {
    from_port   = "80"
    to_port     = "80"
    protocol    = "tcp"
    cidr_blocks = ["10.0.8.0/24"]
  }
  egress {
    from_port   = "443"
    to_port     = "443"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
    egress {
    from_port   = "80"
    to_port     = "80"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  tags = {
    Name = "two-tier-alb-sg"
  }
}

################### Database tier Security gruop ###################
resource "aws_security_group" "two-tier-db-sg" {
  name        = "two-tier-db-sg"
  description = "allow traffic from internet"
  vpc_id      = aws_vpc.three_tier_VPC.id

  ingress {
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.two-tier-ec2-sg.id]
    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.two-tier-ec2-sg.id]
    cidr_blocks     = ["10.0.0.0/16"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "two-tier-db-sg"
  }
}

################### NACL Public subnet ###################
resource "aws_network_acl" "three_tier_Public_NACL" {
  vpc_id = aws_vpc.three_tier_VPC.id

  egress {
    protocol   = "-1"
    rule_no    = 200
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }
  ingress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }
  tags = {
    Name = "Public_NACL"
  }
}
resource "aws_network_acl_association" "public" {
  count         = length(var.three_tier_public_subnet_cidr_blocks)
  network_acl_id = aws_network_acl.three_tier_Public_NACL.id
  subnet_id      = aws_subnet.three_tier_public_subnet[count.index].id
}


################### NACL Private subnet ###################

resource "aws_network_acl" "three_tier_Private_NACL" {
  vpc_id = aws_vpc.three_tier_VPC.id

  egress {
    protocol   = "-1"
    rule_no    = 200
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }
  ingress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }
  tags = {
    Name = "Private_NACL"
  }
}
resource "aws_network_acl_association" "private" {
  count         = length(var.three_tier_private_subnet_cidr_blocks)
  network_acl_id = aws_network_acl.three_tier_Private_NACL.id
  subnet_id      = aws_subnet.three_tier_private_subnet[count.index].id
}
