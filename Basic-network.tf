# Auther : Zubair Khan
# Version : 0.1
# Detail : Please replace the region, CIDR blocks, and other parameters as needed for your specific use case. This Terraform configuration will create the requested 
#          infrastructure with the specified VPC, subnets, Internet Gateway, and route tables. Remember to run terraform init, terraform plan, and terraform apply to
#          deploy this infrastructure.

provider "aws" {
  region = "ap-south-1" # Change to your desired region
  profile = "default"
  shared_config_files      = ["/home/zubair/.aws/config"]
  shared_credentials_files = ["/home/zubair/.aws/credentials"]
}
###################### Create Resources ################################
#  Create VPC
resource "aws_vpc" "three_tier_VPC" {
  cidr_block = var.three_tier_vpc_cidr
  tags = {
    Name = "three_tier_VPC"
  } 
}
# Create public subnets
resource "aws_subnet" "three_tier_public_subnet" {
    vpc_id = aws_vpc.three_tier_VPC.id
    count = length(var.three_tier_public_subnet_cidr_blocks)
    cidr_block = var.three_tier_public_subnet_cidr_blocks[count.index]
    availability_zone = var.availability_zones[count.index]
    map_public_ip_on_launch = true
    depends_on = [aws_vpc.three_tier_VPC]
    tags = {
      Name = "three_tier_public_subnet-${count.index + 1}"  
  }
}
# Create private subnets
resource "aws_subnet" "three_tier_private_subnet" {
    vpc_id = aws_vpc.three_tier_VPC.id
    count = length(var.three_tier_private_subnet_cidr_blocks)
    cidr_block = var.three_tier_private_subnet_cidr_blocks[count.index]
    depends_on = [aws_vpc.three_tier_VPC]
    availability_zone = var.availability_zones[count.index]
    tags = {
      Name = "three_tier_private_subnet-${count.index + 1}"  
  }
}
# Create DB subnets
resource "aws_subnet" "three_tier_DB_subnet" {
    vpc_id = aws_vpc.three_tier_VPC.id
    count = length(var.three_tier_DB_subnet_cidr_blocks)
    cidr_block = var.three_tier_DB_subnet_cidr_blocks[count.index]
    depends_on = [aws_vpc.three_tier_VPC]
    availability_zone = var.availability_zones[count.index]
    tags = {
      Name = "three_tier_DB_subnet-${count.index + 1}"  
  }
}

#  Create Internet Gateway
resource "aws_internet_gateway" "three_tier_IGW" {
  vpc_id = aws_vpc.three_tier_VPC.id
  tags = {
    Name = "three_tier_IGW"
  } 
}

###########  Create public route table
resource "aws_route_table" "three_tier_public_route_table" {
  vpc_id = aws_vpc.three_tier_VPC.id
  tags = {
    Name = "three_tier_public_route_table"
  } 
}
# add routes in public route table
resource "aws_route" "three_tier_public_route" {
  route_table_id         = aws_route_table.three_tier_public_route_table.id
  destination_cidr_block = "0.0.0.0/0"  # Default route
  gateway_id             = aws_internet_gateway.three_tier_IGW.id
  depends_on = [aws_internet_gateway.three_tier_IGW]
}

# Associate public subnets with public route table
resource "aws_route_table_association" "public_subnet_association" {
  count          = 2
  subnet_id      = aws_subnet.three_tier_public_subnet[count.index].id
  route_table_id = aws_route_table.three_tier_public_route_table.id

}
###########  Create private route table
resource "aws_route_table" "three_tier_private_route_table" {
  vpc_id = aws_vpc.three_tier_VPC.id
  tags = {
    Name = "three_tier_private_route_table"
  } 
}
# add routes in public route table
resource "aws_route" "three_tier_private_route" {
  route_table_id         = aws_route_table.three_tier_private_route_table.id
  destination_cidr_block = "0.0.0.0/0"  # Default route
}

# Associate public subnets with private route table
resource "aws_route_table_association" "private_subnet_association" {
  count          = 2
  subnet_id      = aws_subnet.three_tier_private_subnet[count.index].id
  route_table_id = aws_route_table.three_tier_private_route_table.id

}
